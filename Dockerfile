# This is based on itzg/minecraft-server

FROM java:8

MAINTAINER Luke Merry <lmerry@gmail.com>

ENV MODPACK "FTB Beyond"

RUN apt-get update && apt-get install -y wget unzip jq
RUN addgroup --gid 1234 minecraft
RUN adduser --disabled-password --home=/data --uid 1234 --gid 1234 --gecos "minecraft user" minecraft

RUN export FETCH_URL=$(curl -G $(echo "https://ftb-api.herokuapp.com/modpacks/$MODPACK/versions/latest" | sed 's/ /%20/g') | jq -r '.["download_link"]') && \
	mkdir /tmp/feed-the-beast && cd /tmp/feed-the-beast && \
	wget -O FTBServer.zip -c $(echo $FETCH_URL) && \
	ls -al && \
	unzip FTBServer.zip && \
	rm FTBServer.zip && \
	bash -x FTBInstall.sh && \
	chown -R minecraft /tmp/feed-the-beast


USER minecraft

EXPOSE 25565

ADD start.sh /start.sh

VOLUME /data
ADD server.properties /tmp/server.properties
WORKDIR /data

ENV MOTD A Minecraft (FTB Beyond 1.2.1) Server Powered by Docker
ENV LEVEL world
ENV JVM_OPTS -Xms2048m -Xmx4096m

CMD bash -x /start.sh
